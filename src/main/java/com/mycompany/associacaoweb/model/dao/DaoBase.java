/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.associacaoweb.model.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author leola
 */
public class DaoBase {
     private static EntityManagerFactory emf; //= Persistence.createEntityManagerFactory("associacao");
        
    private static EntityManager con; // = emf.createEntityManager();
    public static void close(){
        if(con.isOpen()){
            con.close();
        }
        
    }
    public static EntityManager getCon(){
        if(con== null || !con.isOpen()){
            emf=Persistence.createEntityManagerFactory("associacao");
            con=emf.createEntityManager();
        }
        return con;
    } 
}
