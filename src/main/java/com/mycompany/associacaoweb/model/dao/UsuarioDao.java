/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.associacaoweb.model.dao;

import com.mycompany.associacaoweb.model.Usuario;
import java.sql.SQLException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;

/**
 *
 * @author leola
 */
public class UsuarioDao extends DaoBase{
    
    public Usuario verifyUser(String login, String password) throws SQLException{
        Query q= getCon().createQuery("SELECT u FROM Usuario u Where u.login= :login AND u.password= :password");
        q.setParameter("login", login);
        q.setParameter("password", password);
        try{
        return (Usuario) q.getSingleResult();
        }
        catch(NonUniqueResultException e)
        {
            //throw new SQLException("Foram encontrados 2 ou mais usuarios no banco, contate o administrador");
           return (Usuario) q.getResultList().get(0);
        }catch(NoResultException e){
            return null;
        }
    
        
        
    }
}
