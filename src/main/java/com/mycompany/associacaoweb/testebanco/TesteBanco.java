/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.associacaoweb.testebanco;

import com.mycompany.associacaoweb.model.Usuario;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author leola
 */
public class TesteBanco {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("associacao");
        
        EntityManager em = emf.createEntityManager();
        
        System.out.println("Conectou ");
        
        Usuario u = new Usuario(9, "felipe","lipe","i",false);
        
        
        em.getTransaction().begin();
        
        em.persist(u);
        
        em.getTransaction().commit();
        
      Query q1= em.createQuery("SELECT u FROM Usuario u WHERE u.id = 9");
      List<Usuario>lista1=(List<Usuario>)q1.getResultList();
      for(Usuario usuario : lista1){
          System.out.println("--->"+usuario.getName()+usuario.getLogin()+usuario.getPassword());
      }
    }
   
    
}
