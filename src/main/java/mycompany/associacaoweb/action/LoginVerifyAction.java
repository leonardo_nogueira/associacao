/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mycompany.associacaoweb.action;

import com.mycompany.associacaoweb.controllergenerics.GenericCommander;
import com.mycompany.associacaoweb.model.dao.UsuarioDao;
import com.mycompany.associacaoweb.model.Usuario;
import java.io.IOException;
import java.rmi.ServerException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static jdk.vm.ci.meta.JavaKind.Int;

/**
 *
 * @author leola
 */
public class LoginVerifyAction extends GenericCommander{

    public LoginVerifyAction(boolean isLogado) {
        super(isLogado);
    }

    @Override
    public void executa(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
      
        try {
            String login = request.getParameter("login");
            String password = request.getParameter("password");
            
            Usuario u= new UsuarioDao().verifyUser(login, password);
            if (u != null){
                response.sendRedirect("layout.jsp");
            } 
            else{
                response.sendRedirect("cadastro.jsp");
            }
            //RequestDispatcher rd = request.getRequestDispatcher("layout.jsp");
            //   rd.forward(request, response);
            // }else{RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
            //rd.forward(request, response);
            //}
        } catch (SQLException ex) {
           throw new ServerException(ex.getMessage());
        }
    }
    
}
